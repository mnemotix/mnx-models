@prefix sioc: <http://rdfs.org/sioc/ns#> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix dc: <http://purl.org/dc/terms/> .
@prefix foaf: <http://xmlns.com/foaf/0.1/> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

sioc:
  a owl:Ontology, owl:Thing, owl:NamedIndividual ;
  rdfs:seeAlso <http://rdfs.org/sioc/spec> ;
  owl:versionInfo "Revision: 1.36" ;
  dc:description """SIOC (Semantically-Interlinked Online Communities) is an ontology for describing the information in online communities. 
This information can be used to export information from online communities and to link them together. The scope of the application areas that SIOC can be used for includes (and is not limited to) weblogs, message boards, mailing lists and chat channels."""@en ;
  dc:title "SIOC Core Ontology Namespace"@en .

dc:hasPart a owl:ObjectProperty .
dc:partOf a owl:ObjectProperty .
dc:references a owl:ObjectProperty .
dc:subject a owl:ObjectProperty .
sioc:about
  a owl:ObjectProperty ;
  rdfs:domain sioc:Item ;
  rdfs:comment "Specifies that this Item is about a particular resource, e.g. a Post describing a book, hotel, etc."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "about"@en .

sioc:account_of
  a owl:ObjectProperty ;
  owl:inverseOf foaf:account ;
  rdfs:domain sioc:UserAccount ;
  rdfs:range foaf:Agent ;
  rdfs:comment "Refers to the foaf:Agent or foaf:Person who owns this sioc:UserAccount."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "account of"@en .

sioc:addressed_to
  a owl:ObjectProperty ;
  rdfs:domain sioc:Item ;
  rdfs:comment "Refers to who (e.g. a UserAccount, e-mail address, etc.) a particular Item is addressed to."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "addressed to"@en .

sioc:administrator_of
  a owl:ObjectProperty ;
  owl:inverseOf sioc:has_administrator ;
  rdfs:domain sioc:UserAccount ;
  rdfs:range sioc:Site ;
  rdfs:comment "A Site that the UserAccount is an administrator of."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "administrator of"@en .

sioc:attachment
  a owl:ObjectProperty ;
  rdfs:domain sioc:Item ;
  rdfs:comment "The URI of a file attached to an Item."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "attachment"@en .

sioc:avatar
  a owl:ObjectProperty ;
  rdfs:subPropertyOf foaf:depiction ;
  rdfs:domain sioc:UserAccount ;
  rdfs:comment "An image or depiction used to represent this UserAccount."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "avatar"@en .

sioc:container_of
  a owl:ObjectProperty ;
  rdfs:subPropertyOf dc:hasPart ;
  owl:inverseOf sioc:has_container ;
  rdfs:domain sioc:Container ;
  rdfs:range sioc:Item ;
  rdfs:comment "An Item that this Container contains."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "container of"@en .

sioc:creator_of
  a owl:ObjectProperty ;
  owl:inverseOf sioc:has_creator ;
  rdfs:domain sioc:UserAccount ;
  rdfs:comment "A resource that the UserAccount is a creator of."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "creator of"@en .

sioc:delivered_at
  a owl:ObjectProperty, owl:DatatypeProperty ;
  rdfs:domain sioc:Item ;
  rdfs:range rdfs:Literal ;
  rdfs:isDefinedBy sioc: ;
  rdfs:comment "When this was delivered, in ISO 8601 format."@en ;
  rdfs:label "delivered at"@en .

sioc:discussion_of
  a owl:ObjectProperty ;
  owl:inverseOf sioc:has_discussion ;
  rdfs:range sioc:Item ;
  rdfs:comment "The Item that this discussion is about."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "discussion of"@en .

sioc:earlier_version
  a owl:ObjectProperty, owl:TransitiveProperty ;
  owl:inverseOf sioc:later_version ;
  rdfs:domain sioc:Item ;
  rdfs:range sioc:Item ;
  rdfs:comment "Links to a previous (older) revision of this Item or Post."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "earlier version"@en .

sioc:email
  a owl:ObjectProperty ;
  rdfs:domain sioc:UserAccount ;
  rdfs:comment "An electronic mail address of the UserAccount."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "email"@en .

sioc:embeds_knowledge
  a owl:ObjectProperty ;
  rdfs:domain sioc:Item ;
  rdfs:range <http://www.w3.org/2004/03/trix/rdfg-1/Graph> ;
  rdfs:comment "This links Items to embedded statements, facts and structured content."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "embeds knowledge"@en .

sioc:feed
  a owl:ObjectProperty ;
  rdfs:comment "A feed (e.g. RSS, Atom, etc.) pertaining to this resource (e.g. for a Forum, Site, UserAccount, etc.)."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "feed"@en .

sioc:follows
  a owl:ObjectProperty ;
  rdfs:domain sioc:UserAccount ;
  rdfs:range sioc:UserAccount ;
  rdfs:comment "Indicates that one UserAccount follows another UserAccount (e.g. for microblog posts or other content item updates)."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "follows"@en .

sioc:function_of
  a owl:ObjectProperty ;
  owl:inverseOf sioc:has_function ;
  rdfs:domain sioc:Role ;
  rdfs:comment "A UserAccount that has this Role."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "function of"@en .

sioc:generator
  a owl:ObjectProperty ;
  rdfs:domain sioc:Item ;
  rdfs:comment "A URI for the application used to generate this Item."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "generator"@en .

sioc:group_of
  a owl:ObjectProperty ;
  owl:inverseOf sioc:has_group ;
  rdfs:label "group of"@en ;
  owl:deprecated true ;
  owl:versionInfo "This property has been renamed. Use sioc:usergroup_of instead." .

sioc:has_administrator
  a owl:ObjectProperty ;
  rdfs:domain sioc:Site ;
  rdfs:range sioc:UserAccount ;
  rdfs:comment "A UserAccount that is an administrator of this Site."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "has administrator"@en .

sioc:has_container
  a owl:ObjectProperty ;
  rdfs:subPropertyOf dc:partOf ;
  rdfs:domain sioc:Item ;
  rdfs:range sioc:Container ;
  rdfs:comment "The Container to which this Item belongs."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "has container"@en .

sioc:has_creator
  a owl:ObjectProperty ;
  rdfs:range sioc:UserAccount ;
  rdfs:comment "This is the UserAccount that made this resource."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "has creator"@en .

sioc:has_discussion
  a owl:ObjectProperty ;
  rdfs:domain sioc:Item ;
  rdfs:comment "A discussion that is related to this Item. The discussion can be anything, for example, a sioc:Forum or sioc:Thread, a sioct:WikiArticle or simply a foaf:Document."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "has discussion"@en .

sioc:has_function
  a owl:ObjectProperty ;
  rdfs:range sioc:Role ;
  rdfs:comment "A Role that this UserAccount has."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "has function"@en .

sioc:has_group
  a owl:ObjectProperty ;
  rdfs:label "has group"@en ;
  owl:deprecated true ;
  owl:versionInfo "This property has been renamed. Use sioc:has_usergroup instead." .

sioc:has_host
  a owl:ObjectProperty ;
  rdfs:subPropertyOf sioc:has_space ;
  owl:inverseOf sioc:host_of ;
  rdfs:domain sioc:Container ;
  rdfs:range sioc:Site ;
  rdfs:comment "The Site that hosts this Container."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "has host"@en .

sioc:has_member
  a owl:ObjectProperty ;
  owl:inverseOf sioc:member_of ;
  rdfs:domain sioc:Usergroup ;
  rdfs:range sioc:UserAccount ;
  rdfs:comment "A UserAccount that is a member of this Usergroup."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "has member"@en .

sioc:has_moderator
  a owl:ObjectProperty ;
  owl:inverseOf sioc:moderator_of ;
  rdfs:domain sioc:Forum ;
  rdfs:range sioc:UserAccount ;
  rdfs:comment "A UserAccount that is a moderator of this Forum."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "has moderator"@en .

sioc:has_modifier
  a owl:ObjectProperty ;
  owl:inverseOf sioc:modifier_of ;
  rdfs:range sioc:UserAccount ;
  rdfs:comment "A UserAccount that modified this resource (e.g. Item, Container, Space)."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "has modifier"@en .

sioc:has_owner
  a owl:ObjectProperty ;
  owl:inverseOf sioc:owner_of ;
  rdfs:range sioc:UserAccount ;
  rdfs:comment "A UserAccount that this resource is owned by."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "has owner"@en .

sioc:has_parent
  a owl:ObjectProperty ;
  rdfs:subPropertyOf dc:partOf ;
  owl:inverseOf sioc:parent_of ;
  rdfs:domain sioc:Container ;
  rdfs:range sioc:Container ;
  rdfs:comment "A Container or Forum that this Container or Forum is a child of."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "has parent"@en .

sioc:has_part
  a owl:ObjectProperty ;
  owl:inverseOf sioc:part_of ;
  rdfs:comment "An resource that is a part of this subject."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "has part"@en ;
  owl:deprecated true ;
  owl:versionInfo "This property is deprecated. Use dcterms:hasPart from the Dublin Core ontology instead." .

sioc:has_reply
  a owl:ObjectProperty ;
  rdfs:subPropertyOf sioc:related_to ;
  owl:inverseOf sioc:reply_of ;
  rdfs:domain sioc:Item ;
  rdfs:range sioc:Item ;
  rdfs:comment "Points to an Item or Post that is a reply or response to this Item or Post."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "has reply"@en .

sioc:has_scope
  a owl:ObjectProperty ;
  owl:inverseOf sioc:scope_of ;
  rdfs:domain sioc:Role ;
  rdfs:comment "A resource that this Role applies to."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "has scope"@en .

sioc:has_space
  a owl:ObjectProperty ;
  rdfs:subPropertyOf dc:partOf ;
  owl:inverseOf sioc:space_of ;
  rdfs:range sioc:Space ;
  rdfs:comment "A data Space which this resource is a part of."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "has space"@en .

sioc:has_subscriber
  a owl:ObjectProperty ;
  owl:inverseOf sioc:subscriber_of ;
  rdfs:domain sioc:Container ;
  rdfs:range sioc:UserAccount ;
  rdfs:comment "A UserAccount that is subscribed to this Container."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "has subscriber"@en ;
  rdfs:seeAlso sioc:feed .

sioc:has_usergroup
  a owl:ObjectProperty ;
  owl:inverseOf sioc:usergroup_of ;
  rdfs:domain sioc:Space ;
  rdfs:range sioc:Usergroup ;
  rdfs:comment "Points to a Usergroup that has certain access to this Space."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "has usergroup"@en .

sioc:host_of
  a owl:ObjectProperty ;
  rdfs:subPropertyOf sioc:space_of ;
  rdfs:domain sioc:Site ;
  rdfs:range sioc:Container ;
  rdfs:comment "A Container that is hosted on this Site."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "host of"@en .

sioc:later_version
  a owl:ObjectProperty, owl:TransitiveProperty ;
  rdfs:domain sioc:Item ;
  rdfs:range sioc:Item ;
  rdfs:comment "Links to a later (newer) revision of this Item or Post."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "later version"@en .

sioc:latest_version
  a owl:ObjectProperty ;
  rdfs:domain sioc:Item ;
  rdfs:range sioc:Item ;
  rdfs:comment "Links to the latest revision of this Item or Post."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "latest version"@en .

sioc:likes
  a owl:ObjectProperty ;
  rdfs:comment "Used to indicate some form of endorsement by a UserAccount or Agent of an Item, Container, Space, UserAccount, etc."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "likes"@en .

sioc:link
  a owl:ObjectProperty ;
  rdfs:comment "A URI of a document which contains this SIOC object."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "link"@en .

sioc:links_to
  a owl:ObjectProperty ;
  rdfs:subPropertyOf dc:references ;
  rdfs:comment "Links extracted from hyperlinks within a SIOC concept, e.g. Post or Site."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "links to"@en .

sioc:member_of
  a owl:ObjectProperty ;
  rdfs:domain sioc:UserAccount ;
  rdfs:range sioc:Usergroup ;
  rdfs:comment "A Usergroup that this UserAccount is a member of."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "member of"@en .

sioc:mentions
  a owl:ObjectProperty ;
  rdfs:domain sioc:Item ;
  rdfs:range sioc:UserAccount ;
  rdfs:comment "Refers to a UserAccount that a particular Item mentions."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "mentions"@en .

sioc:moderator_of
  a owl:ObjectProperty ;
  rdfs:domain sioc:UserAccount ;
  rdfs:range sioc:Forum ;
  rdfs:comment "A Forum that a UserAccount is a moderator of."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "moderator of"@en .

sioc:modifier_of
  a owl:ObjectProperty ;
  rdfs:domain sioc:UserAccount ;
  rdfs:comment "A resource that this UserAccount has modified."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "modifier of"@en .

sioc:next_by_date
  a owl:ObjectProperty ;
  owl:inverseOf sioc:previous_by_date ;
  rdfs:domain sioc:Item ;
  rdfs:range sioc:Item ;
  rdfs:comment "Next Item or Post in a given Container sorted by date."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "next by date"@en .

sioc:next_version
  a owl:ObjectProperty ;
  rdfs:subPropertyOf sioc:later_version ;
  owl:inverseOf sioc:previous_version ;
  rdfs:domain sioc:Item ;
  rdfs:range sioc:Item ;
  rdfs:comment "Links to the next revision of this Item or Post."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "next version"@en .

sioc:owner_of
  a owl:ObjectProperty ;
  rdfs:domain sioc:UserAccount ;
  rdfs:comment "A resource owned by a particular UserAccount, for example, a weblog or image gallery."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "owner of"@en .

sioc:parent_of
  a owl:ObjectProperty ;
  rdfs:subPropertyOf dc:hasPart ;
  rdfs:domain sioc:Container ;
  rdfs:range sioc:Container ;
  rdfs:comment "A child Container or Forum that this Container or Forum is a parent of."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "parent of"@en .

sioc:part_of
  a owl:ObjectProperty ;
  rdfs:comment "A resource that the subject is a part of."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "part of"@en ;
  owl:deprecated true ;
  owl:versionInfo "This property is deprecated. Use dcterms:isPartOf from the Dublin Core ontology instead." .

sioc:previous_by_date
  a owl:ObjectProperty ;
  rdfs:domain sioc:Item ;
  rdfs:range sioc:Item ;
  rdfs:comment "Previous Item or Post in a given Container sorted by date."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "previous by date"@en .

sioc:previous_version
  a owl:ObjectProperty ;
  rdfs:subPropertyOf sioc:earlier_version ;
  rdfs:domain sioc:Item ;
  rdfs:range sioc:Item ;
  rdfs:comment "Links to the previous revision of this Item or Post."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "previous version"@en .

sioc:read_at
  a owl:ObjectProperty, owl:DatatypeProperty ;
  rdfs:domain sioc:Item ;
  rdfs:range rdfs:Literal ;
  rdfs:isDefinedBy sioc: ;
  rdfs:comment "When this was read, in ISO 8601 format."@en ;
  rdfs:label "read at"@en .

sioc:reference
  a owl:ObjectProperty ;
  rdfs:domain sioc:Post ;
  rdfs:comment "Links either created explicitly or extracted implicitly on the HTML level from the Post."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "reference"@en ;
  owl:deprecated true ;
  owl:versionInfo "Renamed to sioc:links_to." .

sioc:related_to
  a owl:ObjectProperty ;
  rdfs:comment "Related resources for this resource, e.g. for Posts, perhaps determined implicitly from topics or references."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "related to"@en .

sioc:reply_of
  a owl:ObjectProperty ;
  rdfs:subPropertyOf sioc:related_to ;
  rdfs:domain sioc:Item ;
  rdfs:range sioc:Item ;
  rdfs:comment "Links to an Item or Post which this Item or Post is a reply to."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "reply of"@en .

sioc:respond_to
  a owl:ObjectProperty ;
  rdfs:domain sioc:Item ;
  rdfs:comment "For the reply-to address set in email messages, IMs, etc. The property name was chosen to avoid confusion with has_reply/reply_of (the reply graph)."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "respond to"@en .

sioc:scope_of
  a owl:ObjectProperty ;
  rdfs:range sioc:Role ;
  rdfs:comment "A Role that has a scope of this resource."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "scope of"@en .

sioc:shared_by
  a owl:ObjectProperty ;
  rdfs:domain sioc:Item ;
  rdfs:range sioc:UserAccount ;
  rdfs:comment "For shared Items where there is a certain creator_of and an intermediary who shares or forwards it (e.g. as a sibling Item)."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "shared by"@en ;
  rdfs:seeAlso sioc:sibling .

sioc:sibling
  a owl:ObjectProperty, owl:SymmetricProperty ;
  rdfs:domain sioc:Item ;
  rdfs:range sioc:Item ;
  rdfs:comment "An Item may have a sibling or a twin that exists in a different Container, but the siblings may differ in some small way (for example, language, category, etc.). The sibling of this Item should be self-describing (that is, it should contain all available information)."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "sibling"@en .

sioc:space_of
  a owl:ObjectProperty ;
  rdfs:subPropertyOf dc:hasPart ;
  rdfs:domain sioc:Space ;
  rdfs:comment "A resource which belongs to this data Space."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "space of"@en .

sioc:subscriber_of
  a owl:ObjectProperty ;
  rdfs:domain sioc:UserAccount ;
  rdfs:range sioc:Container ;
  rdfs:comment "A Container that a UserAccount is subscribed to."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "subscriber of"@en ;
  rdfs:seeAlso sioc:feed .

sioc:topic
  a owl:ObjectProperty ;
  rdfs:subPropertyOf dc:subject ;
  rdfs:comment "A topic of interest, linking to the appropriate URI, e.g. in the Open Directory Project or of a SKOS category."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "topic"@en .

sioc:usergroup_of
  a owl:ObjectProperty ;
  rdfs:domain sioc:Usergroup ;
  rdfs:range sioc:Space ;
  rdfs:comment "A Space that the Usergroup has access to."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "usergroup of"@en .

foaf:account a owl:ObjectProperty .
foaf:depiction a owl:ObjectProperty .
dc:date a owl:DatatypeProperty .
dc:description a owl:DatatypeProperty .
dc:title a owl:DatatypeProperty .
sioc:content
  a owl:DatatypeProperty ;
  rdfs:domain sioc:Item ;
  rdfs:range rdfs:Literal ;
  rdfs:comment "The content of the Item in plain text format."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "content"@en .

sioc:content_encoded
  a owl:DatatypeProperty ;
  rdfs:domain sioc:Post ;
  rdfs:range rdfs:Literal ;
  rdfs:comment "The encoded content of the Post, contained in CDATA areas."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "content encoded"@en ;
  owl:deprecated true ;
  owl:versionInfo "This property is deprecated. Use content:encoded from the RSS 1.0 content module instead." .

sioc:created_at
  a owl:DatatypeProperty ;
  rdfs:domain sioc:Post ;
  rdfs:range rdfs:Literal ;
  rdfs:comment "When this was created, in ISO 8601 format."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "created at"@en ;
  owl:deprecated true ;
  owl:versionInfo "This property is deprecated. Use dcterms:created from the Dublin Core ontology instead." .

sioc:description
  a owl:DatatypeProperty ;
  rdfs:domain sioc:Post ;
  rdfs:range rdfs:Literal ;
  rdfs:comment "The content of the Post."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "description"@en ;
  owl:deprecated true ;
  owl:versionInfo "This property is deprecated. Use sioc:content or other methods (AtomOwl, content:encoded from RSS 1.0, etc.) instead." .

sioc:email_sha1
  a owl:DatatypeProperty ;
  rdfs:domain sioc:UserAccount ;
  rdfs:range rdfs:Literal ;
  rdfs:comment "An electronic mail address of the UserAccount, encoded using SHA1."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "email sha1"@en .

sioc:first_name
  a owl:DatatypeProperty ;
  rdfs:domain sioc:UserAccount ;
  rdfs:range rdfs:Literal ;
  rdfs:comment "First (real) name of this User. Synonyms include given name or christian name."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "first name"@en ;
  owl:deprecated true ;
  owl:versionInfo "This property is deprecated. Use foaf:name or foaf:firstName from the FOAF vocabulary instead." .

sioc:id
  a owl:DatatypeProperty ;
  rdfs:range rdfs:Literal ;
  rdfs:comment "An identifier of a SIOC concept instance. For example, a user ID. Must be unique for instances of each type of SIOC concept within the same site."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "id"@en .

sioc:ip_address
  a owl:DatatypeProperty ;
  rdfs:range rdfs:Literal ;
  rdfs:comment "The IP address used when creating this Item, UserAccount, etc. This can be associated with a creator. Some wiki articles list the IP addresses for the creator or modifiers when the usernames are absent."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "ip address"@en .

sioc:last_activity_date
  a owl:DatatypeProperty ;
  rdfs:subPropertyOf dc:date ;
  rdfs:range rdfs:Literal ;
  rdfs:comment "The date and time of the last activity associated with a SIOC concept instance, and expressed in ISO 8601 format. This could be due to a reply Post or Comment, a modification to an Item, etc."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "last activity date"@en .

sioc:last_item_date
  a owl:DatatypeProperty ;
  rdfs:subPropertyOf dc:date ;
  rdfs:domain sioc:Container ;
  rdfs:range rdfs:Literal ;
  rdfs:comment "The date and time of the last Post (or Item) in a Forum (or a Container), in ISO 8601 format."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "last item date"@en .

sioc:last_name
  a owl:DatatypeProperty ;
  rdfs:domain sioc:UserAccount ;
  rdfs:range rdfs:Literal ;
  rdfs:comment "Last (real) name of this user. Synonyms include surname or family name."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "last name"@en ;
  owl:deprecated true ;
  owl:versionInfo "This property is deprecated. Use foaf:name or foaf:surname from the FOAF vocabulary instead." .

sioc:last_reply_date
  a owl:DatatypeProperty ;
  rdfs:subPropertyOf dc:date ;
  rdfs:range rdfs:Literal ;
  rdfs:comment "The date and time of the last reply Post or Comment, which could be associated with a starter Item or Post or with a Thread, and expressed in ISO 8601 format."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "last reply date"@en .

sioc:modified_at
  a owl:DatatypeProperty ;
  rdfs:domain sioc:Post ;
  rdfs:range rdfs:Literal ;
  rdfs:comment "When this was modified, in ISO 8601 format."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "modified at"@en ;
  owl:deprecated true ;
  owl:versionInfo "This property is deprecated. Use dcterms:modified from the Dublin Core ontology instead." .

sioc:name
  a owl:DatatypeProperty ;
  rdfs:range rdfs:Literal ;
  rdfs:comment "The name of a SIOC concept instance, e.g. a username for a UserAccount, group name for a Usergroup, etc."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "name"@en .

sioc:note
  a owl:DatatypeProperty ;
  rdfs:range rdfs:Literal ;
  rdfs:comment "A note associated with this resource, for example, if it has been edited by a UserAccount."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "note"@en .

sioc:num_authors
  a owl:DatatypeProperty ;
  rdfs:range xsd:nonNegativeInteger ;
  rdfs:comment "The number of unique authors (UserAccounts and unregistered posters) who have contributed to this Item, Thread, Post, etc."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "num authors"@en .

sioc:num_items
  a owl:DatatypeProperty ;
  rdfs:domain sioc:Container ;
  rdfs:range xsd:nonNegativeInteger ;
  rdfs:comment "The number of Posts (or Items) in a Forum (or a Container)."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "num items"@en .

sioc:num_replies
  a owl:DatatypeProperty ;
  rdfs:range xsd:nonNegativeInteger ;
  rdfs:comment "The number of replies that this Item, Thread, Post, etc. has. Useful for when the reply structure is absent."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "num replies"@en .

sioc:num_threads
  a owl:DatatypeProperty ;
  rdfs:domain sioc:Forum ;
  rdfs:range xsd:nonNegativeInteger ;
  rdfs:comment "The number of Threads (AKA discussion topics) in a Forum."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "num threads"@en .

sioc:num_views
  a owl:DatatypeProperty ;
  rdfs:range xsd:nonNegativeInteger ;
  rdfs:comment "The number of times this Item, Thread, UserAccount profile, etc. has been viewed."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "num views"@en .

sioc:subject
  a owl:DatatypeProperty ;
  rdfs:domain sioc:Post ;
  rdfs:range rdfs:Literal ;
  rdfs:comment "Keyword(s) describing subject of the Post."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "subject"@en ;
  owl:deprecated true ;
  owl:versionInfo "This property is deprecated. Use dcterms:subject from the Dublin Core ontology for text keywords and sioc:topic if the subject can be represented by a URI instead." .

sioc:title
  a owl:DatatypeProperty ;
  rdfs:domain sioc:Post ;
  rdfs:range rdfs:Literal ;
  rdfs:comment "This is the title (subject line) of the Post. Note that for a Post within a threaded discussion that has no parents, it would detail the topic thread."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "title"@en ;
  owl:deprecated true ;
  owl:versionInfo "This property is deprecated. Use dcterms:title from the Dublin Core ontology instead." .

sioc:Community
  a owl:Class ;
  owl:disjointWith sioc:Item, sioc:Role, sioc:UserAccount ;
  rdfs:comment "Community is a high-level concept that defines an online community and what it consists of."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "Community"@en .

sioc:Container
  a owl:Class ;
  owl:disjointWith sioc:Item, sioc:Role, sioc:User, sioc:UserAccount, sioc:Usergroup ;
  rdfs:comment "An area in which content Items are contained."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "Container"@en .

sioc:Forum
  a owl:Class ;
  rdfs:subClassOf sioc:Container ;
  rdfs:comment "A discussion area on which Posts or entries are made."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "Forum"@en .

sioc:Item
  a owl:Class ;
  owl:disjointWith sioc:Role, sioc:Space, sioc:User, sioc:UserAccount, sioc:Usergroup ;
  rdfs:comment "An Item is something which can be in a Container."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "Item"@en .

sioc:Post
  a owl:Class ;
  rdfs:subClassOf sioc:Item, foaf:Document ;
  rdfs:comment "An article or message that can be posted to a Forum."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "Post"@en .

sioc:Role
  a owl:Class ;
  owl:disjointWith sioc:Space, sioc:User, sioc:UserAccount, sioc:Usergroup ;
  rdfs:comment "A Role is a function of a UserAccount within a scope of a particular Forum, Site, etc."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "Role"@en .

sioc:Site
  a owl:Class ;
  rdfs:subClassOf sioc:Space ;
  rdfs:comment "A Site can be the location of an online community or set of communities, with UserAccounts and Usergroups creating Items in a set of Containers. It can be thought of as a web-accessible data Space."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "Site"@en .

sioc:Space
  a owl:Class ;
  owl:disjointWith sioc:User, sioc:UserAccount, sioc:Usergroup ;
  rdfs:comment "A Space is a place where data resides, e.g. on a website, desktop, fileshare, etc."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "Space"@en .

sioc:Thread
  a owl:Class ;
  rdfs:subClassOf sioc:Container ;
  rdfs:comment "A container for a series of threaded discussion Posts or Items."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "Thread"@en .

sioc:User
  a owl:Class ;
  owl:equivalentClass sioc:UserAccount ;
  rdfs:subClassOf foaf:OnlineAccount ;
  owl:disjointWith sioc:Usergroup ;
  rdfs:comment "UserAccount is now preferred. This is a deprecated class for a User in an online community site."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "User"@en ;
  owl:deprecated true ;
  owl:versionInfo "This class is deprecated. Use sioc:UserAccount from the SIOC ontology instead." .

sioc:UserAccount
  a owl:Class ;
  rdfs:subClassOf foaf:OnlineAccount ;
  owl:disjointWith sioc:Usergroup ;
  rdfs:comment "A user account in an online community site."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "User Account"@en .

sioc:Usergroup
  a owl:Class ;
  rdfs:comment "A set of UserAccounts whose owners have a common purpose or interest. Can be used for access control purposes."@en ;
  rdfs:isDefinedBy sioc: ;
  rdfs:label "Usergroup"@en .

<http://www.w3.org/2004/03/trix/rdfg-1/Graph> a owl:Class .
foaf:Agent a owl:Class .
foaf:Document a owl:Class .
foaf:OnlineAccount a owl:Class .
<http://rdfs.org/sioc/spec> rdfs:label "SIOC Core Ontology Specification" .