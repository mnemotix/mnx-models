@prefix : <http://ns.mnemotix.com/ontologies/2019/8/generic-model/> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix xml: <http://www.w3.org/XML/1998/namespace> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix prov: <http://www.w3.org/ns/prov#> .
@prefix dc: <http://purl.org/dc/elements/1.1/> .

@base <http://ns.mnemotix.com/ontologies/2019/8/project/> .

<http://ns.mnemotix.com/ontologies/2019/8/project/> rdf:type owl:Ontology ;
owl:imports <http://www.w3.org/2001/XMLSchema#>, <http://www.w3.org/1999/02/22-rdf-syntax-ns#>, <http://www.w3.org/2000/01/rdf-schema#>, <http://www.w3.org/ns/prov#>;
  dc:creator "Mnemotix SCIC"@fr ;
  dc:created "2019-10-10T13:29:55.317Z"^^xsd:dateTimestamp;
  rdfs:comment "Mnémotix ontologie to describe projects based on Ressource project for Lafyette Anticipation fundation"@en ;
  rdfs:label "Mnémotix Ressource Project Model"@en ;
  owl:versionInfo "2.0.0" ;
  owl:priorVersion "1.0.2" ;
  rdfs:comment """
  06/09/2023 Version 2.0.0
  # renommé en resource-project
  27/08/2020 Version 1.0.2,
  # Ajout de mnx:isTeamOf propriété inverse de mnx:hasTeam
  26/03/2020 Version 1.0.1,
  # Ajout de mnx:hasAttachment propriété inverse de mnx:hasResource
  """ .



#################################################################
#    Classes
#################################################################

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/Attachment
:Attachment rdf:type owl:Class;
  rdfs:subClassOf :Entity ;
  rdfs:label "Attachment"@en .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/Email
:Email rdf:type owl:Class;
  rdfs:subClassOf :Resource, :Entity ;
  rdfs:label "Email"@en .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/File
:File rdf:type owl:Class;
  rdfs:subClassOf :Resource, :Entity ;
  rdfs:label "File"@en .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/Involvement
:Involvement rdf:type owl:Class;
  rdfs:subClassOf :Entity ;
  rdfs:label "Involvement"@en .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/MaterialEntity
:MaterialEntity rdf:type owl:Class;
  rdfs:subClassOf :Entity ;
  rdfs:label "MaterialEntity"@en .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/ProjectContribution
:ProjectContribution rdf:type owl:Class;
  rdfs:subClassOf :Duration, :TemporalEntity, :MaterialEntity, :Entity ;
  rdfs:label "ProjectContribution"@en .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/Project
:Project rdf:type owl:Class;
  rdfs:subClassOf :Entity ;
  rdfs:label "Project"@en .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/ProjectOutput
:ProjectOutput rdf:type owl:Class;
  rdfs:subClassOf :Entity ;
  rdfs:label "ProjectOutput"@en .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/Resource
:Resource rdf:type owl:Class;
  rdfs:subClassOf :Entity ;
  rdfs:label "Resource"@en .


#################################################################
#    Object Properties
#################################################################

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/hasResource
:hasResource rdf:type owl:ObjectProperty ;
  owl:maxCardinality "1"^^xsd:int ;
  rdfs:domain :Attachment ;
  rdfs:range :Resource;
  owl:inverseOf :isResourceOf.

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/hasAgent
:hasAgent rdf:type owl:ObjectProperty ;
  owl:maxCardinality "1"^^xsd:int ;
  rdfs:domain :Involvement ;
  rdfs:range :Agent .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/hasAuthor
:hasAuthor rdf:type owl:ObjectProperty ;
  rdfs:domain :Person .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/hasParentProject
:hasParentProject rdf:type owl:ObjectProperty ;
  owl:maxCardinality "1"^^xsd:int ;
  rdfs:domain :Project ;
  rdfs:range :Project .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/hasInvolvement
:hasInvolvement rdf:type owl:ObjectProperty ;
  rdfs:domain :ProjectContribution ;
  rdfs:range :Involvement ;
  owl:inverseOf :isInvolvementOf.

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/hasAttachment
:hasAttachment rdf:type owl:ObjectProperty ;
  rdfs:domain :ProjectContribution ;
  rdfs:range :Involvement ;
  owl:inverseOf :isAttachmentOf.

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/hasProject
:hasProject rdf:type owl:ObjectProperty ;
  rdfs:range :ProjectContribution ;
  rdfs:domain :Project ;
  owl:inverseOf :isProjectOf.

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/hasProjectOutput
:hasProjectOutput rdf:type owl:ObjectProperty ;
  rdfs:domain :Project ;
  rdfs:range :ProjectOutput .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/hasTeam
:hasTeam rdf:type owl:ObjectProperty ;
  owl:inverseOf :isTeamOf ;
  owl:maxCardinality "1"^^xsd:int ;
  rdfs:domain :Project ;
  rdfs:range :UserGroup .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/hasPicture
:hasPicture rdf:type owl:ObjectProperty ;
  rdfs:domain :ProjectOutput ;
  rdfs:range :Resource .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/hasMainPicture
:hasMainPicture rdf:type owl:ObjectProperty ;
  owl:maxCardinality "1"^^xsd:int ;
  rdfs:domain :ProjectOutput ;
  rdfs:range :Resource .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/hasRepresentation
:hasRepresentation rdf:type owl:SymmetricProperty ;
  rdfs:domain :ProjectOutput ;
  rdfs:range :ProjectOutput .

#################################################################
#    Data properties
#################################################################

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/filename
:filename rdf:type owl:DatatypeProperty;
  rdfs:domain :File ;
  rdfs:range rdfs:Literal .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/mime
:mime rdf:type owl:DatatypeProperty;
  rdfs:domain :File ;
  rdfs:range rdfs:Literal .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/size
:size rdf:type owl:DatatypeProperty;
  rdfs:domain :File ;
  rdfs:range <http://www.w3.org/2001/XMLSchema#float> .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/role
:role rdf:type owl:DatatypeProperty;
  rdfs:domain :Involvement ;
  rdfs:range rdfs:Literal .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/title
:title rdf:type owl:DatatypeProperty;
  rdfs:range rdfs:Literal .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/description
:description rdf:type owl:DatatypeProperty;
  rdfs:range rdfs:Literal .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/shortDescription
:shortDescription rdf:type owl:DatatypeProperty;
  rdfs:range rdfs:Literal .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/memo
:memo rdf:type owl:DatatypeProperty;
  rdfs:domain :ProjectContribution ;
  rdfs:range rdfs:Literal .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/color
:color rdf:type owl:DatatypeProperty;
  rdfs:domain :Project ;
  rdfs:range rdfs:Literal .

### http://ns.mnemotix.com/ontologies/2019/8/generic-model/image
:image rdf:type owl:DatatypeProperty;
  rdfs:domain :Project ;
  rdfs:range rdfs:Literal .
